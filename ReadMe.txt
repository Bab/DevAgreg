Ce projet contient les sources Latex de certains de mes développements préparés pour l'agrégation de Mathématique option D.

Pour compiler, ouvrez un terminal et tapez 'make' dans le répertoire correspondant.

Tapez 'make clean' pour supprimer tous les fichiers auxiliaires générés par latex.