\documentclass{article}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{faktor}
\usepackage{tikz}
\usepackage[french]{algorithm2e}

\newtheorem{thm}{Théorème}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lemme}{Lemme}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exmp}[thm]{Exemple}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{rapp}[thm]{Rappels}

\newcommand\N{\mathbb{N}}
\newcommand\R{\mathbb{R}}
\newcommand\F{\mathbb{F}}
\newcommand\GL{\mbox{GL}}
\newcommand\Id{\mathit{Id}}
\newcommand\pgcd{\mathrm{pgcd}}

\title{Algorithme de Berlekamp}
\author{Bastien Thomas}
\date{}

\begin{document}
\maketitle

Soit $\F_q$ un corps fini (de cardinal $q$).

Soit $P \in \F_q[X]$ un polynôme unitaire sans facteur carré.

On considère l'application suivante :
\[
\varphi :
\begin{array}{c c c}
  \faktor{\F_q[X]}{(P)} & \longrightarrow & \faktor{\F_q[X]}{(P)} \\
  Q & \longmapsto & Q^q
\end{array}
\]
\begin{thm}
  On a les trois points suivants :
  \begin{enumerate}
  \item L'application $\varphi$ est un endomorphisme du $\F_q$-espace vectoriel $\faktor{\F_q[X]}{(P)}$.
  \item Le nombre de facteurs irréductibles de $P$ est égal à $\dim(\ker(\varphi - \Id))$
  \item Si $P$ n'est pas irréductible, soit $V \in \ker(\varphi - \Id)$ non constant. Alors :
    $$ P = \prod_{a \in \F_q} \pgcd(P, V-a) $$
    Et cette décomposition n'est pas triviale.
  \end{enumerate}
\end{thm}

\begin{proof}
  Montrons les trois points du Théorème :
  \begin{enumerate}
  \item Soit $\lambda \in \F_q$, soit $Q, Q' \in \left(\faktor{\F_q[X]}{(P)}\right)^2$. On a :
    \begin{itemize}
    \item $\varphi(\lambda Q) = \lambda^q Q^q = \lambda \varphi(Q)$.
    \item $\varphi(Q + Q') = \sum_{i=0}^q \binom{q}{i} Q^i {Q'}^{q-i} = {Q'}^q + Q^q + q \sum_{i=1}^{q-1} \frac{(q-1)!}{i! (q-i)!} Q^i {Q'}^{q-i} = \varphi(Q) + \varphi(Q')$
    \end{itemize}
    Donc $\varphi$ est linéaire.
  \item Notons $P = \prod_{i=1}^n P_i$ la décomposition en facteurs irréductibles distincts (hypothèse) de $P$

    Les $\left(P_i\right)_{1 \leq i \leq n}$ sont premiers entre eux. Donc par le Théorème Chinois, il existe un isomorphisme $\psi$ tel que :

    \[
    \psi :
    \begin{array}{c c c}
      \faktor{\F_q[X]}{(P)} & \longrightarrow & \faktor{\F_q[X]}{(P_1)} \times \dots \times \faktor{\F_q[X]}{(P_n)} \\
      Q & \longmapsto & \left(Q \mod P_1, \dots, Q \mod P_n\right)
    \end{array}
    \]

    Comme chaque $P_i$ est irréductible dans $\F_q$, les $\left(\faktor{\F_q[X]}{(P_i)}\right)_{1 \leq i \leq n}$ sont des corps.

    Soit $Q \in \faktor{\F_q[X]}{(P)}$, notons $\psi(Q) = (Q_1, \dots, Q_n)$. On a :
    \begin{align}
      Q \in \ker(\varphi - \Id) & \Leftrightarrow Q^q - Q = 0 \mod P && \\
      & \Leftrightarrow \forall i \in \{1, \dots, n\}, {Q_i}^q - Q_i = 0 \mod P_i && \text{Par bijectivité de $\psi$.} \\
      & \Leftrightarrow \forall i \in \{1, \dots, n\}, Q_i \in \F_q && \text{$q$ racines à $X^q - X$.} \\
      & \Leftrightarrow (Q_1, \dots, Q_n) \in {\F_q}^n &&
    \end{align}

    Donc $\ker(\varphi - \Id) = \psi^{-1}\left({\F_q}^n\right)$, donc, comme $\psi$ peut être vue comme un isomorphisme d'espaces vectoriels, $\dim(\ker(\varphi - \Id)) = n$.

  \item Pour $n > 1$, il existe $V \in \ker(\varphi - \Id)$ non constant.
    
    Notons $\psi(V) = (a_1, \dots, a_n)$, par le point précédent, $(a_i)_{1 \leq i \leq n} \in {\F_q}^n$.

    Pour $a \in \F_q$, $i \in \{1, \dots, n\}$, on a $P_i \mid V - a \Leftrightarrow a_i = a$.

    On a donc $\pgcd(P, V - a) = \prod_{i, a_i = a} P_i$.

    Et donc $P = \prod_{a \in \F_q} \pgcd(P, V - a)$.

    De plus, comme $V$ n'est pas constant, les $a_i$ ne sont pas tous identiques et la décomposition n'est pas triviale.
  \end{enumerate}%
\end{proof}

Pour $P$ sans facteurs carrés, on dispose alors de l'algorithme suivant :
\begin{algorithm}
  \KwData{$P \in \F_q[X]$ de degrés $d$ sans facteurs carrés.}
  \KwResult{$P_1 \times \dots \times P_n$ la décomposition de $P$ en facteurs irréductibles.}
  \BlankLine
  Calculer $M$ la matrice de $\varphi$ sur la base $(1, X, \dots, X^{d-1})$\;
  Appliquer le pivot de Gauss sur $M - I_d$. Extraire $(b_1, \dots, b_n)$ une base du noyau\;
  \If{$n > 1$}{
    Choisir $V$ non constant dans $\{b_1, \dots, b_n\}$\;
    Calculer $(Q_a)_{a \in \F_q}$ où $Q_a = \pgcd(P, V-a)$\;
    Pour chaque $Q_a$ non trivial, calculer récursivement sa décomposition\;
    Renvoyer le produit des décomposition des $Q_a$\;
  }
  \Else{
    $P$ est déjà irréductible\;
  }
\end{algorithm}

$P$ a un facteur carré si et seulement si $P$ et $P'$ ont un facteur en commun.

On souhaite donc étudier $\pgcd(P, P')$. Comme $\F_q$ est de caractéristique non nulle, il faut faire attention au cas suivant :

$\pgcd(P, P') = P$ ssi $P \mid P'$ ssi $P' = 0$ ssi $P$ est de la forme $a_0 + a_1 X^p + a_2 X^{2p} + \dots + a_k X^{kp}$ où $p$ est la caractéristique de $\F_q$. Dans ce cas, $P = {\left(a_0^{\frac{q}{p}} + a_1^{\frac{q}{p}}X + a_2^{\frac{q}{p}}X^2 + \dots + a_k^{\frac{q}{p}}X^k\right)}^p$.

L'algorithme suivant décompose alors $P$ en produit de polynômes sans facteurs carrés :

\begin{algorithm}
  \KwData{$P \in \F_q[X]$ de degrès $d$.}
  \KwResult{$P_1 \times \dots \times P_n$ une décomposition de $P$ en facteurs sans carrés.}
  \BlankLine
  $A = \pgcd(P, P')$\;
  \uIf{$A = 1$}{
    $P$ est sans facteurs carrés\;
  }
  \uElseIf{$A = P$}{
    Calculer $Q$ tel que $Q^p = P$\;
    Récursivement calculer une décomposition pour $Q$\;
  }
  \Else{
    Récursivement calculer une décomposition pour $A$ et pour $\frac{P}{A}$\;
  }
\end{algorithm}

On voit facilement que la complexité globale de l'algorithme est polynomiale en les valeurs de $d$ et $q$, ce qui est bien meilleur qu'une approche exhaustive (en $q^d$).


\end{document}
