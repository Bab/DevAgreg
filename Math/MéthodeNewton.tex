\documentclass{article}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{faktor}
\usepackage{tikz}
\usepackage[french]{algorithm2e}

\newtheorem{thm}{Théorème}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lemme}{Lemme}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exmp}[thm]{Exemple}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{rapp}[thm]{Rappels}

\newcommand\N{\mathbb{N}}
\newcommand\R{\mathbb{R}}
\newcommand\F{\mathbb{F}}
\newcommand\GL{\mathrm{GL}}
\newcommand\diff{d}
\newcommand\id{\mathrm{id}}

\title{Méthode de Newton-Raphson}
\date{}
\author{Bastien Thomas}

\begin{document}
\maketitle
\begin{thm}
  Soit $\Omega$ un ouvert de $\R^n$, soit $f$ une fonction de $\Omega$ dans $\R^n$, et soit $a \in \Omega$. Supposons que :
  \begin{itemize}
  \item $f$ est de classe $C^2$ sur $\Omega$.
  \item $f(a) = 0$.
  \item $\diff_af \in \GL_n(\R)$.
  \end{itemize}

  Alors, il existe un ouvert $U \subset \Omega$ contenant $a$ tel que :
  \begin{itemize}
  \item l'application $g: \left\{\begin{aligned}
    U &\rightarrow U\\
    x &\mapsto x - (\diff_xf)^{-1}(f(x))
  \end{aligned}\right.$
    est bien définie.
  \item les suites définies par $\left\{\begin{aligned}
    u_0 &= x \in U\\
    u_{n+1} &= f(u_n)
  \end{aligned}\right.$ convergent quadratiquement vers $a$.
  \end{itemize}
\end{thm}

Par continuité de $x \mapsto \det(\diff_x(f))$, il existe un voisinage $V$ de $A$ tel que pour tout $x \in V$, $\det(\diff_x(f)) \neq 0$, et $g$ est alors bien définie sur $V$.

On vérifie aisément que $g(a) = a \in V$. Soit $h \in \R_n$ tel que $a+h \in V$.
\begin{align*}
  (\diff_{a+h}f)^{-1} &= \left(\diff_af + \diff_a\left(x \mapsto \diff_xf\right)(h) + o(|h|)\right)^{-1} & \text{Car $f$ est $C^2$} \\
  &= \left(\diff_af + \diff_a^2f(h, \cdot) + o(|h|)\right)^{-1} \\
  &= \left(\diff_af \circ \left( \id + (\diff_af)^{-1} \circ \diff_a^2f(h, \cdot) + o(|h|)\right)\right)^{-1} & \text{Par linéarité de $\diff_af$} \\
  &= \left(\id + (\diff_af)^{-1} \circ \diff_a^2f(h, \cdot) + o(|h|)\right)^{-1} \circ \left(\diff_a(f)\right)^{-1}\\
  &= \left(\id - (\diff_af)^{-1} \circ \diff_a^2f(h, \cdot) +o(|h|)\right) \circ \left(\diff_a(f)\right)^{-1} & \text{Car $(1 + h)^{-1} = 1 - h + o(|h|)$}
\end{align*}

Alors, on obtient :
\begin{align*}
  (\diff_{a+h}f)^{-1}\left(f(a+h)\right) &= (\diff_{a+h}f)^{-1} \left( f(a) + \diff_af(h) + \frac{\diff_a^2f(h, h)}{2} + o\left({|h|}^2\right)\right)\\
  &= (\diff_{a+h}f)^{-1} \circ \diff_af \left(h + \frac{1}{2}\left(\diff_af\right)^{-1}\left(\diff_a^2f(h, h)\right) + o\left({|h|}^2\right)\right)\\
  &= \left(\id - (\diff_af)^{-1} \circ \diff_a^2f(h, \cdot) +o(|h|)\right) \circ \left(h + \frac{1}{2}\left(\diff_af\right)^{-1}\left(\diff_a^2f(h, h)\right) + o\left({|h|}^2\right)\right)\\
  &= h + \frac{1}{2}\left(\diff_af\right)^{-1}\left(\diff_a^2f(h, h)\right) - \left(\diff_af\right)^{-1}\left(\diff_a^2f(h, h)\right) + o\left({|h|}^2\right)\\
  &= h - \frac{1}{2}\left(\diff_af\right)^{-1}\left(\diff_a^2f(h, h)\right) + o\left({|h|}^2\right)
\end{align*}

On obtiens :
\begin{align*}
  g(a+h) - g(a) &= a + h - (\diff_{a+h}f)^{-1}\left(f(a+h)\right) - a \\
  &= h - \left(h - \frac{1}{2}\left(\diff_af\right)^{-1}\left(\diff_a^2f(h, h)\right) + o\left({|h|}^2\right)\right) \\
  &= \frac{1}{2}\left(\diff_af\right)^{-1}\left(\diff_a^2f(h, h)\right) + o\left({|h|}^2\right)
\end{align*}

On obtiens :

\begin{align*}
  |g(a+h) - g(a)| &= \left| \frac{1}{2}\left(\diff_af\right)^{-1}\left(\diff_a^2f(h, h)\right) + o\left({|h|}^2\right) \right|\\
  &\leq \frac{1}{2}\left\|\left(\diff_af\right)^{-1}\right\|\left\|\diff_a^2f\right\| {|h|}^2 + o\left({|h|}^2\right)\\
  &\leq M {|h|}^2 + o\left({|h|}^2\right) & \text{En notant $M = \frac{1}{2}\left\|\left(\diff_af\right)^{-1}\right\|\left\|\diff_a^2f\right\|$}\\
  &\leq \left(M + o(1)\right){|h|}^2
\end{align*}

On paut alors considérer un ouvert $V' \subset V$, avec $a \in V'$ tel que $M' = \sup_{h \in V' \setminus \{a\}}\left(\frac{|g(a+h) - g(a)|}{{|h|}^2}\right) < \infty$.

Pour $n \in \N$, on obtiens :

\begin{align*}
  M'\left|u_{n+1} - a\right| &= M'\left|g(u_n) - a\right| \\
  &\leq M'^2\left|u_n - a\right|^2 \\
  &\leq \left(M'\left|u_n - a\right|\right)^2
\end{align*}

D'où pour tout $n \in \N$,

$$ \left| u_n - a \right| \leq $$


\end{document}
