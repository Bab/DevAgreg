\documentclass{article}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{faktor}
\usepackage{tikz}
\usepackage[french]{algorithm2e}

\newtheorem{thm}{Théorème}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lemme}{Lemme}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exmp}[thm]{Exemple}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{rapp}[thm]{Rappels}

\newcommand\sgn{\mathrm{sgn}}
\newcommand\id{\mathrm{id}}
\newcommand\GS{\mathfrak{S}}
\newcommand\GA{\mathfrak{A}}
\newcommand\N{\mathbb{N}}
\newcommand\R{\mathbb{R}}
\newcommand\F{\mathbb{F}}
\newcommand\GL{\mbox{GL}}
\newcommand\Id{\mathit{Id}}
\newcommand\pgcd{\mathrm{pgcd}}

\title{Simplicité de $\GA_n$}
\author{Bastien Thomas}
\date{}

\begin{document}

\maketitle

Soit $n \geq 5$.

\begin{prop}
  Les $3$-cycles engendrent $\GA_n$.
\end{prop}

\begin{prop}
  Les $3$-cycles sont conjugués modulo $\GA_n$.
\end{prop}

\begin{proof}
  Soit $(a, b, c)$ un $3$-cycle. Montrons que $(1, 2, 3)$ et $(a, b, c)$ sont conjugués par un élément de $\GA_n$.

  Les $3$-cycles $(1, 2, 3)$ et $(a, b, c)$ sont conjugués modulo $\GS_n$. Soit donc $u \in \GS_n$ tel que $u(1, 2, 3)u^{-1} = (a, b, c)$.
  \begin{itemize}
  \item Si $\sgn(u) = 1$, alors $u \in \GA_n$ et c'est bon.
  \item Sinon, comme $n \geq 5$, il existe $c, d \in {\{1, \dots, n\}}^2$ tel que $\left|\{a, b, c, d, e\}\right| = 5$.

    Alors :
    \begin{align*}
      \left[(d, e) u\right] (1, 2, 3) \left[(d, e) u\right]^{-1} &= (d, e) u (1, 2, 3) u^{-1} (d, e) \\
      &= (d, e) (a, b, c) (d, e) \\
      &= (a, b, c)
    \end{align*}
  \end{itemize}

  Et $\sgn\left((d, e) u\right) = 1$ donc $\left((d, e) u\right) \in \GA_n$.
\end{proof}

Soit $H$ distingué dans $\GA_n$, $H \neq \{\id\}$.

\begin{prop}
  Le groupe $H$ contient un $3$-cycle.
\end{prop}
\begin{proof}
  Soit $\sigma \in H \setminus \{\id\}$. Quitte à considérer un $\sigma^k$ au lieu de $\sigma$, on suppose que $\sigma$ est d'ordre un nombre premier $p$.

  Notons alors $\sigma = c_1, \dots, c_r$ où les $c_i$ sont des $p$-cycles à supports à supports disjoints.
  \begin{itemize}
  \item Cas $p = 2$.

    On note $\sigma = (a_1, b_1), \dots, (a_r, b_r)$ avec $r$ pair.

    Notons $\sigma' = (a_r, b_r), \dots, (a_3, b_3), (b_1, b_2), (a_1, a_2)$. Soit $u \in \GS_n$ tel que $u \sigma u^{-1} = \sigma'$. Quitte à considérer $(a_1, a_2) u$ au lieu de $u$, on peut supposer que $u \in \GA_n$. Donc, comme $H$ est distingué dans $\GA_n$, $\sigma' = u \sigma u^{-1} \in H$. Donc $\sigma \sigma' = (a_1, b_2)(a_2, b_1) \in H$.

    Soit $d \notin \{a_1, a_2, b_1, b_2\}$ ($n \geq 5$). Soit $u \in \GS_n$ tel que $u (a_1, b_2)(a_2, b_1) u^{-1} = (a_2, b_1) (a_1, d)$. Quitte à considérer $(a_2, b_1)u$ au lieu de $u$, on suppose que $u \in \GA_n$.

    Alors $(a_2, b_1) (a_1, d) \in H$, donc $(a_1, b_2) (a_2, b_1) (a_2, b_1) (a_1, d) = (a_1, b_2, d) \in H$ ce qui nous permet de conclure.

  \item Si $p$ est impair et $r = 1$, notons $\sigma = (a_1, \dots, a_p)$.

    Soit $u \in \GS_n$ tel que $u \sigma u^{-1} = (a_p, a_{p-1}, \dots, a_3, a_1, a_2) = \sigma'$.
    \begin{itemize}
    \item Si $u \in \GA_n$, alors $\sigma' \in H$ et $\sigma \sigma' = (a_1, a_p, a_2) \in H$.
    \item Sinon, $(a_1, a_3) u \in \GA_n$ et $(a_1, a_3) u \sigma u^{-1} (a_1, a_3) = (a_p, a_{p-1}, \dots, a_4, a_1, a_3, a_2) = \sigma''$. Alors $\sigma'' \in H$ donc $\sigma \sigma'' = (a_1, a_p, a_3) \in H$.
    \end{itemize}
    
  \item Si $p$ est impair et $r \geq 2$.

    Notons $\sigma' = c_r^{-1} c_{r-1}^{-1} \dots c_2^{-1} c_1$. Soit $u \in \GS_n$ tel que $u \sigma u^{-1} = \sigma'$.
    \begin{itemize}
    \item Si $u \in \GA_n$, $\sigma' \in H$.
    \item Sinon, notons $c_1 = (x_1, \dots, x_p)$ et $c_2 = (y_1, \dots, y_p)$. Notons $w = (x_1, y_1)(x_2, y_2)\dots(x_p, y_p)$. On vérifie que $w \sigma w^{-1} = \sigma$ et que $\sgn(w) = -1$.

      Alors, $uw \in \GA_n$ et $uw \sigma {(uw)}^{-1} = \sigma'$. Donc $\sigma' \in H$.
    \end{itemize}

    Dans tous les cas, $\sigma' \in H$, donc $\sigma \sigma' = c_1^2 \in H$ et $c_1^2$ est un $p$-cycle car $p \neq 2$. On conclue alors la preuve en se ramenant au cas précédent. 
  \end{itemize}
\end{proof}

Alors $H$ contient un $3$-cycle. Il les contient donc tous par la proposition 2. Donc $H$ contient le groupe engendré par les $3$-cycles qui n'est autre que $\GA_n$ par la proposition 1. Donc $H = \GA_n$.

Donc $\GA_n$ est simple pour $n \geq 5$.


\end{document}
