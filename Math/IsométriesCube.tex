\documentclass[9pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}

\newcommand\Z{\mathbb{Z}}
\newcommand\N{\mathbb{N}}
\newcommand\Id{\mbox{Id}}
\newcommand\Image{\mbox{Im}}


\title{Etude du groupe des isométries du cube}
\date{}
\author{Bastien Thomas}

\begin{document}
\maketitle

Soit  $C = \{A_1, A_2, A_3, A_4, B_1, B_2, B_3, B_4\}$ un cube tel que sur la Figure \ref{cube}.

Soit $I^+(C)$ l'ensemble des isométries affines positives qui préservent $C$.

Faisons agir $I^+(C)$ sur $D = \{D_1, D_2, D_3, D_4\} = \left\{ \{A_1, B_3\}, \{A_2, B_4\}, \{A_3, B_1\}, \{A_4, B_2\} \right\}$ l'ensemble des `grandes diagonales' de $C$ par action naturelle $\varphi$ :

Nous allons montrer que $\varphi$ est bien définie et est un isomorphisme.

\begin{align*}
  \varphi : I^+(C) &\to \frak{S}(D) \approx \frak{S}_4 \\
  g &\mapsto \left( \{A_i , B_j\} \mapsto \{g(A_i), g(B_j)\}\right)
\end{align*}

\begin{figure}[h]
  \centering
  \begin{tikzpicture}[scale=4]
    \def\dx{0.45}
    \def\dy{0.35}
    
    \coordinate (A1) at (0, 1);
    \coordinate (A2) at (\dx, 1+\dy);
    \coordinate (A3) at (1+\dx, 1+\dy);
    \coordinate (A4) at (1, 1);
    \coordinate (B1) at (0, 0);
    \coordinate (B2) at (\dx, \dy);
    \coordinate (B3) at (1+\dx, \dy);
    \coordinate (B4) at (1, 0);

    \draw[green!40, thick] (A1) -- (B3);
    \draw[red!40, thick] (A2) -- (B4);
    \draw[blue!40, thick] (A3) -- (B1);
    \draw[yellow!40, thick] (A4) -- (B2);
    
    \begin{scope}
      \draw (A1) -- (A2);
      \draw (A2) -- (A3);
      \draw (A3) -- (A4);
      \draw (A4) -- (A1);
      \draw (A1) -- (B1);
      \draw (A3) -- (B3);
      \draw (A4) -- (B4);
      \draw (B3) -- (B4);
      \draw (B4) -- (B1);
    \end{scope}
    \begin{scope}[dashed]
      \draw (A2) -- (B2);
      \draw (B1) -- (B2);
      \draw (B2) -- (B3);
    \end{scope}

    \node[above left] at (A1) {$A_1$};
    \node[above left] at (A2) {$A_2$};
    \node[above left] at (A3) {$A_3$};
    \node[above left] at (A4) {$A_4$};
    \node[above left] at (B1) {$B_1$};
    \node[above left] at (B2) {$B_2$};
    \node[above left] at (B3) {$B_3$};
    \node[above left] at (B4) {$B_4$};

  \end{tikzpicture}
  \caption{Cube représenté avec ses grandes diagonales}
  \label{cube}
\end{figure}


\begin{itemize}
\item Les diagonales de $D$ sont les plus grandes distances présente dans $C$. L'image d'une de ces diagonale est donc nécessairement une autre de ces diagonales. Donc $\varphi$ est bien définie.
\item Soit $g \in I^+(C)$ tel que $\forall i \in \{1, 2, 3, 4\}, g(D_i) = D_i$. Montrons que $g = \Id$.

  \begin{itemize}
  \item Si $\exists i \in \{1, 2, 3, 4\}, g(A_i) = A_i$, quite à renommer les sommets, supposons $g(A_1) = A_1$, et donc $g(B_3) = B_3$.

    Comme $d(A_1, A_2) \neq d(A_1, B_4)$, il vient $g(A_2) = A_2$, on montre de même que $g(A_4) = A_4$.

    Or, $(A_1, A_2, A_4, B_1)$ forme un repère affine de l'espace.

    $g$ fixe donc un repère affine et $g = \Id$.
    
  \item Sinon, notons $O$ le centre de $C$ et $s_O$ la symétrie de centre $O$.
    Alors $s_O$ et $g$ agissent de la même manière sur $C$ qui contient un repère affine, donc $g = s_O$. Mais $s_O$ est une isométrie négative. Donc ce cas est impossible.
  \end{itemize}
  Donc $\varphi$ est bien injective, et $I^+(C) \subseteq \frak{S}_4$.

\item Soient $D_i, D_j$ deux diagonales de $C$. Quitte à renommer les sommets, on suppose que $D_i = D_1$ et $D_j = D_2$. Montrons qu'il existe $g \in I^+(C)$ tel que $\varphi(g)$ soit la transposition de $\tau(D_1, D_2)$.

  Notons $M$ le milieu du segment $A_1A_2$. Notons $N$ le milieu du segment $B_3B_4$. On vérifie que la symétrie d'axe $(M, N)$ convient.

  Donc, $\Image(\varphi)$ contient toutes les transpositions, donc $\varphi$ est bien surjective, et $\frak{S}_4 \subseteq I^+(C)$.
\end{itemize}

On a montré que $\frak{S}_4 \approx I^+(C)$.

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=4]
    \def\dx{0.45}
    \def\dy{0.35}
    
    \coordinate (A1) at (0, 1);
    \coordinate (A2) at (\dx, 1+\dy);
    \coordinate (A3) at (1+\dx, 1+\dy);
    \coordinate (A4) at (1, 1);
    \coordinate (B1) at (0, 0);
    \coordinate (B2) at (\dx, \dy);
    \coordinate (B3) at (1+\dx, \dy);
    \coordinate (B4) at (1, 0);

    \begin{scope}[gray]
      \draw (A1) -- (A2);
      \draw (A2) -- (A3);
      \draw (A3) -- (A4);
      \draw (A4) -- (A1);
      \draw (A1) -- (B1);
      \draw (A3) -- (B3);
      \draw (A4) -- (B4);
      \draw (B3) -- (B4);
      \draw (B4) -- (B1);
    \end{scope}
    \begin{scope}[gray, dashed]
      \draw (A2) -- (B2);
      \draw (B1) -- (B2);
      \draw (B2) -- (B3);
    \end{scope}

    \coordinate (C1) at (0.5+\dx/2, 1+\dy/2);
    \coordinate (C2) at (\dx/2, 0.5+\dy/2);
    \coordinate (C3) at (0.5+\dx, 0.5+\dy);
    \coordinate (C4) at (1+\dx/2, 0.5+\dy/2);
    \coordinate (C5) at (0.5, 0.5);
    \coordinate (C6) at (0.5+\dx/2, \dy/2);

    \begin{scope}
      \draw (C1) -- (C2);
      \draw (C1) -- (C4);
      \draw (C1) -- (C5);
      \draw (C4) -- (C5);
      \draw (C5) -- (C2);
      \draw (C2) -- (C6);
      \draw (C4) -- (C6);
      \draw (C5) -- (C6);
    \end{scope}

    \begin{scope}[dashed]
      \draw (C1) -- (C3);
      \draw (C2) -- (C3);
      \draw (C3) -- (C4);
      \draw (C3) -- (C6);
    \end{scope}
    
\end{tikzpicture}
\caption{Une isométrie du cube induit une isométrie de l'octaèdre}
\label{octa dans cube}
\end{figure}

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=3]
    \def\dx{0.45}
    \def\dy{0.35}
    
    \coordinate (A1) at (-1/3-\dx/3, 1/3-\dy/3);
    \coordinate (A2) at (-1/3+\dx/3, 1/3+\dy/3);
    \coordinate (A3) at (1/3+\dx/3, 1/3+\dy/3);
    \coordinate (A4) at (1/3-\dx/3, 1/3-\dy/3);
    \coordinate (B1) at (-1/3-\dx/3, -1/3-\dy/3);
    \coordinate (B2) at (-1/3+\dx/3, -1/3+\dy/3);
    \coordinate (B3) at (1/3+\dx/3, -1/3+\dy/3);
    \coordinate (B4) at (1/3-\dx/3, -1/3-\dy/3);

    \begin{scope}
      \draw (A1) -- (A2);
      \draw (A2) -- (A3);
      \draw (A3) -- (A4);
      \draw (A4) -- (A1);
      \draw (A1) -- (B1);
      \draw (A3) -- (B3);
      \draw (A4) -- (B4);
      \draw (B3) -- (B4);
      \draw (B4) -- (B1);
    \end{scope}
    \begin{scope}[dashed]
      \draw (A2) -- (B2);
      \draw (B1) -- (B2);
      \draw (B2) -- (B3);
    \end{scope}

    \coordinate (C1) at (0, 1);
    \coordinate (C2) at (-1, 0);
    \coordinate (C3) at (\dx, \dy);
    \coordinate (C4) at (1, 0);
    \coordinate (C5) at (-\dx, -\dy);
    \coordinate (C6) at (0, -1);

    \begin{scope}[gray]
      \draw (C1) -- (C2);
      \draw (C1) -- (C4);
      \draw (C1) -- (C5);
      \draw (C4) -- (C5);
      \draw (C5) -- (C2);
      \draw (C2) -- (C6);
      \draw (C4) -- (C6);
      \draw (C5) -- (C6);
    \end{scope}

    \begin{scope}[gray, dashed]
      \draw (C1) -- (C3);
      \draw (C2) -- (C3);
      \draw (C3) -- (C4);
      \draw (C3) -- (C6);
    \end{scope}
    
\end{tikzpicture}
\caption{Une isométrie de l'octaèdre induit une isométrie du cube}
\label{cube dans octa}
\end{figure}

On remarque que les $6$ milieux des faces d'un cube forment un octaèdre régulier (Figure \ref{octa dans cube}). Une isométrie qui préserve le cube préserve alors aussi cet octaèdre, et l'application qui à une isométrie du cube associe l'isométrie de l'octaèdre correspondante est un morphisme injectif (l'octaèdre contient un repère affine).

Donc les isométries du cube sont inclues dans celles de l'octaèdre.

Réciproquement, on peut inclure un cube dans un octaèdre de la même manière (Figure \ref{cube dans octa}). Donc les isométries de l'octaèdre sont inclues dans celles du cube.

Donc les isométries du cube sont isomorphes à celles de l'octaèdre.

\paragraph{Source :} Caldéro Germoni, `Histoire Hédoniste de Groupes et Géométrie', tome 1.

\end{document}
