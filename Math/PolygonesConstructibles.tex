\documentclass{article}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{faktor}
\usepackage{tikz}
\usepackage[french]{algorithm2e}

\newtheorem{thm}{Théorème}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lemme}{Lemme}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exmp}[thm]{Exemple}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{rapp}[thm]{Rappels}

\newcommand\N{\mathbb{N}}
\newcommand\Z{\mathbb{Z}}
\newcommand\Q{\mathbb{Q}}
\newcommand\R{\mathbb{R}}
\newcommand\K{\mathbb{K}}
\newcommand\F{\mathbb{F}}
\newcommand\Id{\mathit{Id}}
\newcommand\GL{\mbox{GL}}
\newcommand\Aut{\mathrm{Aut}}

\newcommand\pgcd{\mathrm{pgcd}}

\title{Polygones réguliers constructibles}
\author{Bastien Thomas}
\date{}

\begin{document}
\maketitle

\begin{thm}
Un complexe $z$ est constructible à la règle et au compas ssi il existe une tour d'extensions de corps $\Q = \K_0 \hookrightarrow \K_1 \hookrightarrow \dots \hookrightarrow \K_n$ avec $z \in \K_n$ et pour $0 \leq i < n$, $\left[\K_{i+1} : \K_i\right] = 2$.
\end{thm}

\begin{thm}
Un polygone régulier à $n$ coté est constructible à la règle et au compas ssi $n$ est de la forme $n = 2^\alpha p_1 \dots p_k$. Où les $(p_i)$ sont des premiers de Fermat distincts (de la forme $2^{2^n}+1$).
\end{thm}

\begin{proof}
  On note $\mathcal{P}_n$ la propriété : \emph{Le polygone régulier à $n$ cotés est constructible}.
  
  Quelques remarques préalables :
  \begin{itemize}
  \item On a $\mathcal{P}_n$ ssi le point d'affixe $e^{\frac{2 i \pi}{n}}$ est constructible.
  \item Pour tous $k$, $\mathcal{P}_{2^k}$ par bisection successives.
  \item Si $\mathcal{P}_n$ et $\mathcal{P}_m$ avec $\pgcd(n, m) = 1$, alors, par relation de Bézout, $\mathcal{P}_{n m}$.
  \end{itemize}
  
Ces points justifient le fait que l'on s'intéresse aux nombres $\xi = e^{\frac{2 i \pi}{p^\alpha}}$ avec $p$ premier et $\alpha \in \N^*$.

On veut montrer l'équivalence $\xi$ est constructible ssi $p$ est un premier de Fermat et $\alpha = 1$.

\paragraph{Sens direct :} Si $\xi = e^{\frac{2 i \pi}{p^\alpha}}$ est constructible, cela signifie que $\left[\Q(\xi) : \Q\right] = 2^k$ pour un certain $k$ dans $\N$.

Or, le polynôme minimal de $\xi$ est le $n$-ième polynôme cyclotomique de degrè $\varphi(p^\alpha) = p^{\alpha - 1}(p - 1)$.

On obtient alors $p^{\alpha - 1}(p - 1) = 2^k$, d'où $\alpha = 1$ et $p = 2^k$.

Montrons que $k$ est une puissance de $2$. Notons $k = \lambda 2^\alpha$ avec $\lambda$ impair. Alors :

$$p = \left(2^{2^\alpha}\right)^\lambda + 1 = \left(2^{2^\alpha} + 1\right) \left(\left(2^{2^\alpha}\right)^{\lambda - 1} - \left(2^{2^\alpha}\right)^{\lambda - 2} + \dots - 2^{2^\alpha} + 1 \right)$$

Comme $p$ est premier, ce n'est possible que si $\lambda = 1$.

\paragraph{Réciproque :} 
Soit $p = 2^n + 1$ avec $n = 2^\beta$ un nombre premier de Fermat. On note $\xi = e^{\frac{2 i \pi}{p}}$.

Le polynôme minimal de $\xi$ est le polynôme cyclotomique $\Phi_p$ de degrès $\varphi(p) = 2^n$.

Posons $G = \Aut_\Q\left(\Q(\xi)\right)$. Montrons que $G$ est cyclique.

Pour $g \in G$, on a $\Phi_p(g(\xi)) = g(\Phi_p(\xi)) = 0$. Donc $g(\xi) \in \{\xi, \xi^2, \dots, \xi^{p-1}\}$. De plus, un choix d'image pour $\xi$ détermine entièrement un élément de $G$.

En particulier, soit $i$ un générateur de $\left(\faktor{\Z}{p\Z}\right)^*$, il existe un unique élément $g_0$ de $G$ tel que $g_0(\xi) = \xi^i$.

On en déduit alors que $G = \left\{\Id, g_0, g_0^2, \dots, g_0^{p-2}\right\}$, car cela énumère bien toutes les possibilités pour l'image de $\xi$.

Pour $i \in \{0, \dots, n\}$, posons $\K_i = \ker\left(g_0^{2^i} - \Id\right)$.

Vérifions que les $\Q = \K_0 \hookrightarrow \dots \hookrightarrow \K_n = \Q(\xi)$ forment une tour d'extensions de degrès $2$.

\begin{itemize}
\item Soit $z \in \K_0$. Notons $z = \sum_{i=0}^{p-2} \lambda_i g_0^i(\xi)$ la décomposition de $z$ dans une paermutation de la base $\left(\xi, \dots, \xi^{p-1}\right)$.

  Mais $g(z) = z$, donc $\sum_{i=0}^{p-2} \lambda_i g_0^{i+1}(\xi) = \sum_{i=0}^{p-2} \lambda_i g_0^i(\xi)$. Donc $\lambda_0 = \lambda_1 = \dots = \lambda_{p-2}$.

  Donc $z = \lambda_0 \sum_{i=0}^{p-2} g_0^{i+1}(\xi) = \lambda_0 \sum_{i = 1}^{p-1} \xi^i = - \lambda_0 \in \Q$.
  
  Donc $\K_0 = \Q$.

\item Comme $g_0^{2^n} = \Id$, on a bien $\K_n = \Q(\zeta)$.

\item Pour $i \in \{0, \dots, n-1\}$, pour $z \in \K_i$, $g^{2^{i+1}}(z) = g^{2^i}\left(g^{2^i}(z)\right) = g^{2^i}(z) = z$. Donc $\K_i \subset \K_{i+1}$.
  
\item Comme $[\K_n : \K_{n-1}] \dots [\K_1 : \K_0] = [\Q(\xi) : \Q] = 2^n$, il suffit de montrer que pour $i \in \{0, \dots, n-1\}$, $[\K_{i+1} : \K_i] \geq 2$, et donc que $\K_{i+1} \not\subset \K_i$.

  Considérons $z = \sum_{h=0}^{2^{n-i-1} - 1} g^{2^{i+1}h}(\xi)$.

  On a $g^{2^i}(z) = \sum_{h=0}^{2^{n-i-1} - 1} g^{2^{i+1}h + 2^i}(\xi) \neq z$ car pour $h = 0$, $g(2^i)(\xi)$ apparaît dans $g^{2^i}(z)$ mais pas dans $z$.

  En revanche, $g^{2^{i+1}}(z) = \sum_{h=0}^{2^{n-i-1} - 1} g^{2^{i+1}(h+1)}(\xi) = z$.

  Donc $z \in \K_{i+1}$, mais $z \notin \K_i$.
\end{itemize}

Ceci conclue alors la preuve.


\end{proof}

\end{document}

