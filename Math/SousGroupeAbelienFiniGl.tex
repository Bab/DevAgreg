\documentclass{article}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{faktor}
\usepackage{tikz}
\usepackage[french]{algorithm2e}

\newtheorem{thm}{Théorème}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lemme}{Lemme}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exmp}[thm]{Exemple}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{rapp}[thm]{Rappels}

\newcommand\sgn{\mathrm{sgn}}
\newcommand\id{\mathrm{id}}
\newcommand\GS{\mathfrak{S}}
\newcommand\GA{\mathfrak{A}}
\newcommand\N{\mathbb{N}}
\newcommand\Z{\mathbb{Z}}
\newcommand\R{\mathbb{R}}
\newcommand\C{\mathbb{C}}
\newcommand\F{\mathbb{F}}
\newcommand\GL{\mbox{GL}}
\newcommand\Id{\mathit{Id}}
\newcommand\pgcd{\mathrm{pgcd}}

\title{Sous-groupe abéliens finis de $\GL_n(\C)$}
\author{Bastien Thomas}
\date{}

\begin{document}
\maketitle

Soit $G$ un sous groupe abélien fini de $\GL_n(\C)$. Notons $G \simeq \faktor{\Z}{d_1\Z} \oplus \dots \oplus \faktor{\Z}{d_s\Z}$ avec $1 \neq d_1 \mid \dots \mid d_s = m$ (théorème de structure des groupes abéliens finis).

Notons $D_m = \{\mathrm{diag}(z_1, \dots, z_n) \mid \forall i, z_i^m = 1\}$.

\begin{prop} On a :
  \begin{enumerate}
  \item Il existe $P \in \GL_n(\C)$ tel que $P G P^{-1} \subset D_m$.
  \item On a $s \leq n$ donc tout sous groupe abélien fini de $\GL_n(\C)$ est engendré par au plus $n$ éléments.
  \item Si $s = n$ et $d_1 = d_2 = \dots = d_s = m$, alors il existe $P \in \GL_n(\C)$ tel que $P G P^{-1} = D_m$.
  \end{enumerate}
\end{prop}

\begin{proof} Montrons chacun des points.
  \begin{enumerate}
  \item Soit $A \in G$, on a $A^m = I_n$ et $X^m - 1$ est scindé à racines simples dans $\C$. Donc $A$ est diagonalisable et ses valeurs propres sont racines $m$-ièmes de l'unité. Le groupe $G$ est alors un ensemble de matrices diagonalisables qui commutent deux à deux. Il existe donc un $P \in \GL_n(\C)$ qui diagonalise tous les éléments de $G$. Comme les valeus propres des éléments de $G$ sont des racines $m$-ièmes de l'unité, on en déduit le résultat.

  \item Pour $H$ un groupe abélien et $p$ un nombre premier, on note $H_p = \{h \in H \mid h^p = 1\}$, c'est un sous-groupe de $H$. De plus, pour $H$ et $L$ deux groupes abéliens, on vérifie aisément que :
    \begin{itemize}
    \item Si $H \subset L$, $H_p \subset L_p$.
    \item ${(H \times L)}_p = H_p \times L_p$.
    \item Si $H$ est cyclique d'ordre mutiple de $p$, alors $H_p$ est cyclique d'ordre $p$.
    \end{itemize}
    Soit $p$ un nombre premier tel que $p \mid d_1 \mid \dots \mid d_s = m$. On déduit des points précédents que $G_p \simeq \left(\faktor{\Z}{p\Z}\right)^s$, et ${\left(D_m\right)}_p \simeq \left(\faktor{\Z}{p\Z}\right)^n$.
    
    Soit $P \in \GL_n(\C)$ tel que $P G P^{-1} \subset D_m$. Alors, ${\left(P G P^{-1}\right)}_p = \left(P G_p P^{-1}\right) \subset {\left(D_m\right)}_p$. Donc $\left(\faktor{\Z}{p\Z}\right)^s \subset \left(\faktor{\Z}{p\Z}\right)^n$ modulo isomorphisme, donc $s \leq n$.

  \item Soit $P \in \GL_n(\C)$ tel que $P G P^{-1} \subset D_m$. Comme $\left| P G P^{-1} \right| = |G| = m^n$ et $D_m = m^n$, on obtient bien $PGP^{-1} = D_m$.
    
  \end{enumerate}
\end{proof}

Pour $d_1 \mid \dots \mid d_s$ avec $s \leq n$, il existe toujours un sous-groupe abélien fini de $\GL_n(\C)$ isomorphe à $\faktor{\Z}{d_1\Z} \oplus \dots \oplus \faktor{\Z}{d_s\Z}$ (considérer $\left\{ \mathrm{diag}(z_1, \dots, z_S, 1, \dots, 1) \mid \forall i, z_i^{d_i} = 1\right\}$).

Il existe des sous-groupes abéliens finis de $\GL_n(\C)$ ayant les mêmes invariants de similitudes, mais qui ne sont pas conjugués, prenons $m = 36$, $n = 2$ et posons $\omega = e^{\frac{2i\pi}{36}}$ :

\begin{align*}
G &= \left\{\mathrm{diag}(\omega^{2n}, \omega^{3m}) \mid n, m \in \Z^2\right\}\\
H &= \left\{\mathrm{diag}(\omega^{6n}, \omega^{m}) \mid n, m \in \Z^2\right\}
\end{align*}

On a bien $G \simeq H \simeq \faktor{\Z}{6\Z} \oplus \faktor{\Z}{36\Z}$.

Par contre si $P = \left(\begin{array}{cc}
  a & b\\
  c & d
\end{array}\right)$ vérifie $PG = HP$, alors en particulier, on obtient que pour $n, m \in \Z^2$, il existe $n', m' \in \Z^2$ tel que :
\begin{align*}
  a\omega^{2n} &= a\omega^{6n'} \\
  2n &\equiv 6n' \mod(36) & \text{ou $a = 0$}\\
  3n' - n &\equiv 0 \mod(36) & \text{ou $a = 0$}
\end{align*}

Pour $n = 1$ et $a\neq 0$, on obtiens une absurdité, donc $a = 0$.

En utilisant $b z^{3m} = b z^{6n'}$, on obtiens de même que $b = 0$.

Donc $P \notin \GL_2(\C)$, ce qui montre bien que $G$ et $H$ ne sont pas conjugués.

\end{document}
