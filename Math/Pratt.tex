\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage[linesnumbered, french]{algorithm2e}
\usepackage{tikz}
\usepackage{hyperref}


\newcommand\Z{\mathbb{Z}}
\newcommand\N{\mathbb{N}}
\newcommand\K{\mathbb{K}}
\newcommand\C{\mathbb{C}}
\newcommand\Q{\mathbb{Q}}
\newcommand\Ker{\mbox{Ker}}
\newcommand\ordre{\mbox{ordre}}
\newcommand\ppcm{\mbox{ppcm}}
\newcommand\pgcd{\mbox{pgcd}}
\newcommand\Aut{\mbox{Aut}}
\newcommand\class[1]{\overline{#1}}

\newtheorem{thm}{Théorème}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lemme}{Lemme}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exmp}[thm]{Exemple}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{rapp}[thm]{Rappels}

\begin{document}
\title{Certificat de Pratt (Prime est NP)}
\author{Bastien Thomas}
\date{}
\maketitle

\begin{lemme}
  Soit $n > 1$, $n$ est premier si et seulement si il existe $x \in \Z / n\Z$ tel que :
  \begin{itemize}
  \item $x^{n-1} = 1$ dans $\Z / n\Z$.
  \item Pour tout $p$ premier divisant $n-1$, $x^{\frac{n-1}{p}} \neq 1$ dans $\Z / n\Z$.
  \end{itemize}
\end{lemme}

\begin{proof}
  \begin{itemize}
  \item Si $n$ est premier, il suffit de choisir pour $x$ un générateur de $(\Z / n\Z)^*$.
  \item Réciproquement, si un tel $x$ existe, alors il est inversible dans $\Z / n\Z$. On se place donc dans $(\Z / n\Z)^*$.
  
  Les conditions du Lemme sont équivalentes à dire que le sous-groupe engendré par $x$ dans $(\Z / n\Z)^*$ est d'ordre exactement $n-1$.

  Donc l'ordre de $(\Z / n\Z)^*$ vaut $n-1$, donc $\Z / n\Z$ est un corps,  donc $n$ est premier.
  \end{itemize}
\end{proof}

\begin{defn}
On définit alors inductivement un ensemble $\mathcal{P} \subset \N \cup \N^3$ (Syntaxe) de la manière suivante :
\begin{itemize}
\item $(m, a, 1) \in \mathcal{P}$ pour tout $m > 1$, $a \geq 1$.
\item si $(m, a, x) \in \mathcal{P}$ et $p \in \mathcal{P}$ et $a^{\frac{m-1}{p}} \not \equiv 1 \mod m$, alors $(m, a, xp) \in \mathcal{P}$.
\item si $(m, a, m-1) \in \mathcal{P}$ et $a^{m-1} \equiv 1 \mod m$, alors $m \in \mathcal{P}$.
\end{itemize}

On définit une preuve comme une suite finie d'éléments de $\mathcal{P}$, chaque élément obtenu des précédents par une des règles ci-dessus.
\end{defn}

\begin{prop}
$m \in \mathcal{P}$ ssi $m$ est premier, et il existe alors une preuve contenant $m$.
\end{prop}

\begin{proof}

On définit aussi l'ensemble $\mathcal{S} \subset \N \cup \N^3$ (Sémantique) de la manière suivante :
\begin{itemize}
\item $(m, a, x) \in \mathcal{S}$ ssi pour tout facteur premier $p$ de $x$, $a^{\frac{m-1}{p}} \not\equiv 1 \mod m$.
\item $m \in \mathcal{S}$ ssi $m$ est premier. 
\end{itemize}

\begin{description}
\item[Correction :] On vérifie aisément par induction que $\mathcal{P} \subset \mathcal{S}$.

\item[Complétude partielle :] Réciproquement, tout $m$ premier appartient à $\mathcal{P}$.

  En effet, par récurrence sur $m$, notons $p_1, \dots, p_k$ la décomposition en facteurs premiers de $m-1$, et notons $A_1, A_k$ des preuves contenant respectivement $p_i$ (hypothèse de récurrence). Prenons $a$ un générateur de $(\Z / m\Z)^*$. Alors $A_1, \dots, A_k, (m, a, 1), (m, a, p_1), \dots, (m, a, p_1 \dots p_k), m$ est une preuve contenant $m$.
\end{description}
\end{proof}

\begin{prop}
  Il existe une preuve content $m$ en moins de $6 \log_2(m) - 4$ étapes.
\end{prop}

\begin{proof}
  Par récurence sur $m$ :
  \begin{itemize}
  \item Si $m = 2$, alors $(2, 1, 1), 2$ est une preuve contenant $2$. Et $6 \log_2(2) - 4  = 2$.
  \item Si $m = 3$, alors $(2, 1, 1), 2, (3, 2, 1), (3, 2, 2), 3$ est une preuve contenant $3$, et $5 < 6 \log_2(3) - 4$.
  \item Sinon, notons $p_1, \dots, p_k$ la décomposition en facteurs premiers de $m-1$, avec $k \geq 2$. Par hypothèse de récurrence, on obtient $p_i \in \mathcal{P}$ en $6 \log_2(p_i) - 4$ étapes.
    Alors, une preuve de $m$ peut être obtenue en $\sum_{i = 1}^k \left(6 \log_2(p_i) - 4\right) + k + 2 = 6 \log_2(m-1) - 3k + 2 \leq 6 \log_2(m) - 4$ étapes. 
  \end{itemize}
\end{proof}

\paragraph{Conséquences :} On peut fournir un certificat de petite taille permettant à un ordinateur de vérifier rapidement qu'un nombre est premier.

On obtient $\mbox{ PRIME } \in \mbox{ NP } \cap \mbox{ coNP }$, L'appartenance à coNP est intuitive, et l'algorithme non déterministe consistant à générer une preuve de taille plus petite que $6 \log_2(m) - 4$ puis à la vérifier résout le problème en temps polynomial.

Remarque : un article datant de 2002 montre que $\mbox{ PRIME } \in \mbox{ P }$. Toutefois des certificats de la forme de ceux de Pratt sont encore utilisés pour des raisons de performances.

Source : \url{http://users.encs.concordia.ca/~chvatal/notes/ppp.pdf}

\end{document}
