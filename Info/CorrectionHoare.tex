\documentclass{article}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{esvect}
\usepackage{tikz}
\usepackage[linesnumbered, french]{algorithm2e}

\newtheorem{thm}{Théorème}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lemme}{Lemme}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exmp}[thm]{Exemple}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{rapp}[thm]{Rappels}

\title{Correction de la logique de Hoare.}
\date{}
\author{Bastien Thomas}


\begin{document}
\maketitle

\begin{lemme}
  Soit $A$ une formule, $\sigma$ une assignation, $x$ une variable et $n$ un entier. Si $\sigma, e \Rightarrow n$, alors on a l'équivalence entre :
  \begin{itemize}
  \item $\sigma \models A[x \leftarrow e]$
  \item et $\sigma[x \leftarrow n] \models A$
  \end{itemize}
\end{lemme}
\begin{proof}
  Montrons par induction sur les expressions arithmétiques que pour toute expression $a$ et entier $m$, $\sigma[x \leftarrow n], a \Rightarrow m$ si et seulement si $\sigma, a[x \leftarrow e] \Rightarrow m$.
  \begin{itemize}
  \item Si $a = y \neq x$, c'est bon.
  \item Si $a = x$, alors $a[x \leftarrow e] = e$, et on a bien $\sigma[x \leftarrow n], x \Rightarrow m$ si et seulement si $\sigma, n \Rightarrow m$ si et seulement si $m = n$.
  \item Si $a = a_1 . a_2$ avec $.$ un opérateur.
    \begin{itemize}
    \item $\sigma[x \leftarrow n], a \Rightarrow m$
    \item ssi il existe $m_1$ et $m_2$ avec $m = m_1 . m_2$ et $\sigma[x \leftarrow n], a_1 \Rightarrow m_1$ et $\sigma[x \leftarrow n], a_2 \Rightarrow m_2$
    \item ssi il existe $m_1$ et $m_2$ avec $m = m_1 . m_2$ et $\sigma, a_1[x \leftarrow e] \Rightarrow m_1$ et $\sigma, a_2[x \leftarrow e] \Rightarrow m_2$
    \item ssi $\sigma, a_1[x \leftarrow e] . a_2[x \leftarrow e] \Rightarrow m$
    \item ssi $\sigma, a[x \leftarrow e] \Rightarrow m$
    \end{itemize}
  \end{itemize}

  Alors, pour toute proposition de la forme $p = a_1 \mathcal{R} a_2$ (où $\mathcal{R} \in \{=, <, >, \leq, \geq\}$),
  \begin{itemize}
  \item $\sigma[x \leftarrow n] \models a_1 \mathcal{R} a_2$
  \item ssi il existe $m_1, m_2$ tels que $\sigma[x \leftarrow n], a_1 \models m_1$ et $\sigma[x \leftarrow n], a_2 \models m_2$ et $m_1 \mathcal{R} m_2$.
  \item ssi il existe $m_1, m_2$ tels que $\sigma, a_1[x \leftarrow e] \models m_1$ et $\sigma, a_2[x \leftarrow e] \models m_2$ et $m_1 \mathcal{R} m_2$.
  \item ssi $\sigma \models p[x \leftarrow e]$
  \end{itemize}

  On termine alors facilement la démonstration par induction sur $A$.
  
\end{proof}

\begin{thm}
  Pour tout programme $S$, toutes valuations $\sigma_1$ et $\sigma_2$, et toutes formules $A$ et $B$, si :
  \begin{itemize}
  \item $\vdash A, S, B$
  \item $\sigma_1, S \Rightarrow \sigma_2$
  \item $\sigma_1 \models A$
  \end{itemize}

  Alors $\sigma_2 \models B$.
\end{thm}

\begin{proof}
  Montrons ce résultat par induction sur l'arbre de preuve de $\vdash A, S, B$.
  \begin{itemize}
  \item Cas :
    $$\overline{\vdash A, \text{skip}, A}$$
    Alors, comme $\sigma_1, \text{skip} \Rightarrow \sigma_2$ ne peut être obtenu que par la règle :
    $$\overline{\sigma, \text{skip} \Rightarrow \sigma}$$
    On a $\sigma_1 = \sigma_2$ donc $\sigma_2 \models A$.
  \item Cas :
    $$\overline{\vdash A[x \leftarrow e], x := e, A}$$
    Comme $\sigma_1, x := e \Rightarrow \sigma_2$ ne peut être obtenu que par :
    $$\frac{\sigma, e \Rightarrow n}{\sigma, x := e \Rightarrow \sigma[x \leftarrow n]}$$
    On en déduit que $\sigma_2 = \sigma_1[x \leftarrow n]$ et que $\sigma_1, e \Rightarrow n$.

    On déduit le résultat par le Lemme.

  \item Cas :
    $$\frac{\vdash A, S_1, B \quad \vdash B, S_2, C}{\vdash A, (S_1; S_2), C}$$

    On a alors :

    $$\frac{\sigma_1, S_1 \Rightarrow \sigma_2 \quad \sigma_2, S_2 \Rightarrow \sigma_3}{\sigma_1, (S_1; S_2) \Rightarrow \sigma_3}$$

    Par hypothèses d'induction, comme $\sigma_1 \models A$ et $\vdash A, S_1, B$, $\sigma_2 \models B$. Mais comme $\vdash B, S_2, C$, on a : $\sigma_3 \models C$.

  \item Cas :
    $$\frac{\vdash (A \wedge b), S_1, C \quad  \vdash (A \wedge \neg b), S_2, C)}{\vdash A, (\text{if } b \text{ then } S_1 \text{ else } S_2), C}$$

    \begin{itemize}
    \item Ou bien :
      $$ \frac{ \sigma_1 \models b \quad \sigma_1, S_1 \Rightarrow \sigma_2 }{\sigma_1, (\text{if } b \text{ then } S_1 \text{ else } S_2) \Rightarrow \sigma_2} $$
      Alors $\sigma_1 \models b$ et $\sigma_1 \models A$ donc $\sigma_1 \models A \wedge b$, donc, par hypothèse d'induction, comme $\sigma_1, S_1 \Rightarrow \sigma_2$ et $\vdash (A \wedge b), S_1, C$, on a : $\sigma_2 \models C$.
    \item Ou bien :
      $$ \frac{ \sigma_1 \models \neg b \quad \sigma_1, S_2 \Rightarrow \sigma_2 }{\sigma_1, (\text{if } b \text{ then } S_1 \text{ else } S_2) \Rightarrow \sigma_2} $$

      Alors, la preuve est identique.
    \end{itemize}

  \item Cas :
    $$\frac{\vdash (A \wedge b), S, A}{\vdash A, (\text{while } b \text{ do } S \text{ end}), A \wedge \neg b}$$

    Montrons par induction sur l'arbre de dérivation de la sémantique que si $\sigma_1 \models A$ et si $\sigma_1, (\text{while } b \text{ do } S \text{ end}) \Rightarrow \sigma_2$ alors $\sigma_2 \models A \wedge \neg b$ :
    \begin{itemize}
    \item Cas :
      $$ \frac{\sigma_1 \models \neg b}{\sigma_1, (\text{while } b \text{ do } S \text{ end}) \Rightarrow \sigma_1} $$

      Alors $\sigma_1 \models A$ et $\sigma_1 \models \neg b$ donc $\sigma_1 \models A \wedge \neg b$.

    \item Cas :
      $$ \frac{\sigma_1 \models b \quad \sigma_1, S \Rightarrow \sigma_2 \quad \sigma_2, (\text{while } b \text{ do } S \text{ end}) \Rightarrow \sigma_3 }{\sigma_1, (\text{while } b \text{ do } S \text{ end}) \Rightarrow \sigma_3} $$

      Alors, $\sigma_1 \models b$ et $\sigma_1 \models A$ donc $\sigma_1 \models A \wedge b$.

      Alors, comme $\sigma_1, S \Rightarrow \sigma_2$ et $\vdash (A \wedge b), S, A$, par hypothèses d'induction (première induction), $\sigma_2 \models A$.

      Alors, comme $\sigma_2, (\text{while } b \text{ do } S) \Rightarrow \sigma_3$, par hypothèse d'induction (deuxième induction), $\sigma_3 \models A \wedge \neg b$.
    \end{itemize}
    
  \item Cas :
    $$ \frac{\vdash A \rightarrow B \quad \vdash B, S, C \quad \vdash C \rightarrow D}{\vdash A \rightarrow D}$$
    Si $\sigma_1 \models A$, comme $A \rightarrow B$ est vraie, $\sigma_1 \models B$, alors par hypothèse d'induction, $\sigma_2 \models C$ donc, comme $C \rightarrow D$ est vraie, $\sigma_2 \models D$.

  \end{itemize}
  
\end{proof}


\end{document}
