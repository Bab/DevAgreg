\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage[]{algorithm2e}

\title{Arbres Binaires Optimaux}
\author{Bastien Thomas}
\date{}

\begin{document}
\maketitle

\section{Situation et Formalisation}

On se donne
\begin{itemize}
\item $k_1, \dots, k_n$ un ensemble ordonné de clefs, $k_1 < k_2 < \dots < k_n$.
\item $p_1, \dots, p_n$ les probabilités de recherche des $k_1, \dots, k_n$.
\item $q_0, \dots, q_n$ tels que $q_i$ corresponde à la probabilité que l'on recherche une clef strictement comprise entre $k_i$ et $k_{i+1}$.
\end{itemize}

On souhaite construire un arbre binaire de recherche $\mathcal{A}$ sur les $(k_i)_{1 \leq i \leq n}$ qui minimise le temps moyen de recherche d'une clef.

On ajoute à $\mathcal{A}$ des feuilles notées $d_0, \dots, d_n$ qui modélisent les clefs absentes de l'arbre.

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[circle, draw] (k1) at (0, 1.5) {$k_1$};
    \node[circle, draw] (k2) at (1, 3) {$k_2$};
    \node[circle, draw] (k3) at (2, 1.5) {$k_3$};
    \node[circle, draw] (k4) at (3, 4.5) {$k_4$};
    \node[circle, draw] (k5) at (5, 3) {$k_5$};

    \node[rectangle, draw, black!50] (d0) at (-0.5, 0) {$d_0$};
    \node[rectangle, draw, black!50] (d1) at (0.5, 0) {$d_1$};
    \node[rectangle, draw, black!50] (d2) at (1.5, 0) {$d_2$};
    \node[rectangle, draw, black!50] (d3) at (2.5, 0) {$d_3$};
    \node[rectangle, draw, black!50] (d4) at (4.5, 1.5) {$d_4$};
    \node[rectangle, draw, black!50] (d5) at (5.5, 1.5) {$d_5$};

    \draw (k4) -- (k2);
    \draw (k4) -- (k5);
    \draw (k2) -- (k1);
    \draw (k2) -- (k3);

    \draw[black!50] (k1) -- (d0);
    \draw[black!50] (k1) -- (d1);
    \draw[black!50] (k3) -- (d2);
    \draw[black!50] (k3) -- (d3);
    \draw[black!50] (k5) -- (d4);
    \draw[black!50] (k5) -- (d5);
  \end{tikzpicture}
\end{figure}

La grandeur que l'on cherche à minimiser peut alors s'écrire:

\begin{equation}\label{cout}
  \begin{split}
    E(X_{\mathcal{A}}) &= \sum_{i = 1}^n p_i\left(\mbox{prof}_{\mathcal{A}}(k_i) + 1\right) + \sum_{i = 0}^n q_i\left(\mbox{prof}_{\mathcal{A}}(d_i) + 1\right) \\
    &= \left(\sum_{i = 1}^n p_i + \sum_{i = 0}^n q_i\right) + \sum_{i = 1}^n p_i\mbox{prof}_{\mathcal{A}}(k_i) + \sum_{i = 0}^n q_i\mbox{prof}_{\mathcal{A}}(d_i)
  \end{split}
\end{equation}

Où $X_{\mathcal{A}}$ correspond au temps de recherche d'une clef.

\section{Relation de Récurrence}

Si $\mathcal{A}$ a pour racine $k_r$, alors on peut réécrire (\ref{cout}) en
\begin{equation*}
  \begin{split}
    E(X_{\mathcal{A}}) &= \left(\sum_{i = 1}^{r-1} p_i + \sum_{i = 0}^{r-1} q_i\right) + \sum_{i = 1}^{r-1} p_i\mbox{prof}_{\mathcal{A}}(k_i) + \sum_{i = 0}^{r-1} q_i\mbox{prof}_{\mathcal{A}}(d_i)\\
    & \quad + p_r \\
    & \quad + \left(\sum_{i = r+1}^n p_i + \sum_{i = r}^n q_i\right) + \sum_{i = r+1}^n p_i\mbox{prof}_{\mathcal{A}}(k_i) + \sum_{i = r}^n q_i\mbox{prof}_{\mathcal{A}}(d_i)\\
    &= \left(\sum_{i = 1}^{r-1} p_i + \sum_{i = 0}^{r-1} q_i\right) + \sum_{i = 1}^{r-1} p_i\left(\mbox{prof}_{\mathcal{A}}(k_i) - 1\right) + \sum_{i = 0}^{r-1} q_i\left(\mbox{prof}_{\mathcal{A}}(d_i) - 1\right)\\
    & \quad + \sum_{i = 1}^n p_i + \sum_{i = 0}^n q_i\\
    & \quad + \left(\sum_{i = r+1}^n p_i + \sum_{i = r}^n q_i\right) + \sum_{i = r+1}^n p_i\left(\mbox{prof}_{\mathcal{A}}(k_i) - 1\right) + \sum_{i = r}^n q_i\left(\mbox{prof}_{\mathcal{A}}(d_i) - 1\right)
  \end{split}
\end{equation*}

En notant $\mathcal{B}$ et $\mathcal{C}$ les fils gauche et droit de $\mathcal{A}$ on obtient:
\begin{equation}\label{rec cout}
  E(X_{\mathcal{A}}) = E(X_{\mathcal{B}}) + \left(\sum_{i = 1}^n p_i + \sum_{i = 0}^n q_i\right) +  E(X_{\mathcal{C}})
\end{equation}

On obtient immédiatement que si $\mathcal{A}$ est optimal, alors ses fils gauche et droit le sont aussi pour leur clefs respectives.

On introduit alors les notations suivantes:
\begin{itemize}
\item $e(i, j)$ pour $0 \leq i - 1 \leq j \leq n$ correspond au coût de l'arbre optimal sur les clefs $(k_i, \dots, k_j)$ (et $(d_{i-1}, \dots, d_j))$.
\item $w(i, j) = \sum_{k = i}^j p_k + \sum_{k = i-1}^j q_k$ pour $0 \leq i - 1 \leq j \leq n$
\end{itemize}

On obtiens alors la relation de récurrence pour $1 \leq i \leq j \leq n$:

\begin{align}
  \label{rec0}
  e(i, i-1) & = q_{i-1}\\
  \label{rec1}
  e(i, j) & = \min_{i \leq r \leq j}\left(e(i, r-1) + e(r+1, j) + w(i, j)\right)
\end{align}

\section{Algorithme}

On utilise la programmation dynamique.

On travaille sur ces tableaux:
\begin{itemize}
\item \texttt{E[$1 \leq i \leq n, i-1 \leq j \leq n$]} contient les valeurs de $e(i, j)$.
\item \texttt{W[$1 \leq i \leq n, i-1 \leq j \leq n$]} contient les valeurs de $w(i, j)$.
\item \texttt{R[$1 \leq i \leq n, i \leq j \leq n$]} contient l'indice de la racine de l'arbre optimal sur les clefs $k_i, \dots, k_j$.
\end{itemize}

Notons que \texttt{R} contient suffisamment d'information pour retrouver l'arbre optimal, ce sera donc le résultat de notre fonction.

\begin{algorithm}[H]
  \KwData{P[$1 \leq i \leq n$], Q[$1 \leq i \leq n$]}
  \KwResult{\texttt{R[$1 \leq i \leq n, i \leq j \leq n$]}}
  \BlankLine
  \emph{Déclaration des tableaux}\;
  E[$1 \leq i \leq n, i-1 \leq j \leq n$]\;
  W[$1 \leq i \leq n, i-1 \leq j \leq n$]\;
  R[$1 \leq i \leq n, i \leq j \leq n$]\;
  \BlankLine
  \emph{Initialisation}\;
  \For{$i\leftarrow 1$ \KwTo $n$}{
    E[$i, i-1$] $\leftarrow$ Q[$i-1$]\;
    W[$i, i-1$] $\leftarrow$ Q[$i-1$]\;
  }
  \emph{Boucle sur $k = j-i$}\;
  \For{$k\leftarrow 0$ \KwTo $n$}{
    \For{$i\leftarrow 1$ \KwTo $n-k$}{
      $j\leftarrow i + k$\;
      W[$i, j$] $\leftarrow$ W[$i, j-1$] $+$ P[$j$] $+$ Q[$j$]\; 
      $r_m \leftarrow 0$\;
      $e_m \leftarrow +\infty$\;
      \For{$r\leftarrow i$ \KwTo $j$}{
        $e \leftarrow$ E[$i, r-1$] $+$ E[$r+1, j$] $+$ W[$i, j$]\;
        \If{$e \leq e_m$}{
          $e_m \leftarrow e$\;
          $r_m \leftarrow r$\;
        }
      }
      E[$i, j$] $\leftarrow e_m$\;
      R[$i, j$] $\leftarrow r$\; 
    }
  }
\end{algorithm}

La complexité de l'algorithme est clairement en $\mathcal{O}(n^3)$.

\end{document}

