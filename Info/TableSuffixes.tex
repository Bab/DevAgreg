\documentclass{article}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage[linesnumbered, french]{algorithm2e}

\newtheorem{thm}{Théorème}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{lemme}{Lemme}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exmp}[thm]{Exemple}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{rapp}[thm]{Rappels}

\newcommand\SA{\mbox{SA}}

\title{Construction linéaire de la table des suffixes.}
\date{}
\author{Bastien Thomas}

\begin{document}
\maketitle

\paragraph{Notations :}
Soit $t = t_0 \dots t_{n-1}$ un texte sur un alphabet ordonné $\Sigma = \{1, \dots, m\}$ on suppose $m \leq n$. On pose $t_n = 0$.

On note $s_i = t_i \dots t_n$ le suffixe de $t$ commençant à la position $i$.

On souhaite construire la permutation SA de $(0, \dots, n)$ telle que $\SA[i] = j$ ssi $s_j$ est le $i$-ème suffixe de $t$ dans l'ordre lexicographique.

On dit qu'un suffixe $s_i$ est \textit{grand} si $s_i > s_{i+1}$, sinon on dit qu'il est \textit{petit}. (Le cas $s_i = s_{i+1}$ est impossible.)

\paragraph{Présentation globale de l'algorithme.}

L'algorithme fonctionne de la manière suivante :

\begin{algorithm}[H]
  \KwData{$t = t_0 \dots t_{n-1}$}
  \KwResult{$\SA[0 \dots n]$}
  \BlankLine
  \If{Toutes les lettres de $t$ sont différentes}{
    $t$ est une permutation, renvoyer $t^{-1}$\;
  }
  \Else{
    Calculer un tableau $I$ tel que $I[i] = 1$ ssi $s_i$ est grand\;
    \If{Il y a moins de suffixes petits que grands}{
      Notons $l_1, \dots, l_k$ l'ensemble des $0$ de $I$\;
      Construire la liste de mots $W_P = t[l_1\dots l_2]\#, \dots, t[l_{k-1}\dots l_k]\# $ où $\# $ est plus grand que toutes les autres lettres de $\Sigma$\;
      Trier $W_P$ afin de construire une liste d'entiers $W_P'$ correspondants à $W_P$ \;
      Construire récursivement la table des suffixes $\SA_P$ de $W_P'$\;
      En déduire les positions des petits suffixes dans $\SA$\;
      Compléter $\SA$ par programation dynamique\;
    }
    \Else{
      Faire la même chose avec les grands suffixes\;
    }
  }
  
\end{algorithm}


Nous allons alors détailler les étapes de l'algorithme.

\paragraph{Ligne 5}
On peut calculer $I$ en temps linéaire par une passe de droite à gauche.

\paragraph{Ligne 9}
La taille totale de $W_P$ est majorée par $3n$. En utilisant un algorithme de tri de mots adapté, on peut construire une permutation $S$ qui trie $W_L$ en $O(n)$. On peut alors associer un entier unique préservant l'ordre à chacun des mots de $W$ en une passe de $W_L[S[0] \dots S[k-1]]$.

\paragraph{Ligne 11}
On a les deux lemmes suivants :

\begin{lemme}
  Si $s_i$ est grand, $s_j$ est petit et si $t_i = t_j$, alors $s_i < s_j$.
\end{lemme}
\begin{proof}
  Par hypothèses, on peut noter $s_i = b^kas_i'$ et $s_j = b^lcs_j'$ avec $a < b < c$.
  Alors quelque soit le cas $k < l$, $k = l$ ou $k > l$, on obtient le résltat.
\end{proof}

\begin{lemme}
  $\SA(W_P') = \SA(W_P) = \SA_P(t)$ où $\SA_P(t)$ correspond à la table des petits suffixes de $t$. 
\end{lemme}
\begin{proof}
  L'égalité $\SA(W_P') = \SA(W_P)$ vient de la construction de $W_P'$.

  Soient $s_i$ et $s_j$ deux petits suffixes de $\SA$. Supposons $s_i < s_j$.

  Notons $w_i\# = t_i\dots t_{i'}\#$ et $w_j\# = t_j\dots t_{j'}\#$ les mots de $W_P$ correspondants.
  \begin{itemize}
  \item Si $w_i \neq w_j$, montrons que $w_i\# < w_j\#$.
    \begin{itemize}
    \item $w_i$ et $w_j$ diffère en une lettre, alors nécessairement $w_i < w_j$ et on obtient bien le résultat.
    \item $w_i$ ne peut être préfixe de $w_j$, en effet, $s_{i'}$ est petit, $s_{j' + i' - i}$ est grand ($w_i \neq w_j$), et $t_{i'} = t_{j' + i' - i}$, donc, par le lemme précédent, $s_{j' + i' - i} < s_{i'}$ et donc $s_j < s_i$ ce qui contredit l'hypothèse.
    \item Si $w_j$ est préfixe propre de $w_i$, alors $w_j\# > w_i\#$ car $\# > t_{j + i' - i}$ et on obtient bien le résultat.
    \end{itemize}
  \item Sinon, on observe que pour tout mot $w$, $w_i\#, \dots, w_k\# < w_j\#, \dots w_k\#$ ssi $w, w_i\#, \dots, w_k\# < w, w_j\#, \dots w_k\#$, et on se ramène au cas précédent en considérant un plus grand préfixe commun.
  \end{itemize}
  
\end{proof}

Alors, le Lemme 2 nous dit que l'on a trié les petits suffixes de $t$. Tandis que le Lemme 1 nous dit comment les placer dans $\SA$ : considérons l'intervalle de $\SA$ constitué des suffixes commençant par la lettre $a$. Il suffit de placer les petits suffixes en fin d'intervalle.

\paragraph{Ligne 12}
On considère l'algorithme suivant :

\begin{algorithm}[H]
  \KwData{$t$, $I$, $\SA$ initialisé avec les petits préfixes}
  \KwResult{$\SA[0 \dots n]$ complété}
  \BlankLine
  \For{$i \leftarrow 0$ \KwTo $n-1$}{
    \If{$\SA[i] = 0$ ou $I[\SA[i] - 1] = 0$}{
      continue\;
    }
    $c \leftarrow t[\SA[i] - 1]$\;
    $j \leftarrow $ première case libre dans l'intervalle de $c$\;
    $SA[j] \leftarrow i-1$\;
  }
\end{algorithm}

\begin{itemize}
\item On ne lit jamais de case non initialisée. En effet, un suffixe non initialisé est nécessairement grand, et son succésseur apparaît donc avant dans $\SA$.
\item L'algorithme est correct. En effet, soit $as_i$ et $as_j$ deux grands suffixes commençant par la même lettre $a$. alors, si la case de $as_i$ a été remplis avant celle de $as_j$, cela signifie que $s_i$ apparaît avant $s_j$ dans $\SA$, et donc que $s_i < s_j$, donc $as_i < as_j$.
\item L'algorithme a une complexité en $O(n)$.
\end{itemize}

\paragraph{Complexité globale}

Sans les appels récursifs, l'algorithme est linéaire.

Si on ajoute les appels, la complexité est alors majorée par $c(n)$ tel que :

$c(n) = C (n + 1) + c\left(\left\lfloor\frac{n}{2}\right\rfloor\right) = C (n + 1) + C \left(\left\lfloor\frac{n}{2}\right\rfloor + 1\right) + \dots + C (1 + 1) \leq C(2n + n) = O(n)$


\end{document}
