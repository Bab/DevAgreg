\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage[]{algorithm2e}

\title{Distance de Levenstein}
\date{}
\author{Bastien Thomas}

\begin{document}

\maketitle

\section{Ce que je pense mettre dans le plan.}

\paragraph{Définition: Sous-Séquence} Une \emph{sous-séquence} $(y_i)_{0 \leq i < m} \in \Sigma^m$ d'une séquence $(x_i)_{0 \leq i < n} \in \Sigma^n$ est obtenue en supprimant des caractères de $(x_i)_{0 \leq i < n}$. On a donc en particulier $m \leq n$.

Par exemple, $brcaabr$ est une sous-séquence de $abracadabra$.

\paragraph{Définition: Alignement} Soit $-$ un caractère n'appartenant pas à $\Sigma$, un \emph{alignement} de $(x_i)_{0 \leq i < n} \in \Sigma^n$ et de $(y_i)_{0 \leq i < m} \in \Sigma^m$ est la donnée de deux séquences $(u_i)_{0 \leq i < k}$ et $(v_i)_{0 \leq i < k}$ dans $\left( \Sigma \cup \{-\} \right)^k$ de même longueur avec :
\begin{itemize}
\item $(x_i)_{0 \leq i < n}$ sous-séquence de $(u_i)_{0 \leq i < k}$.
\item $(y_i)_{0 \leq i < m}$ sous-séquence de $(v_i)_{0 \leq i < k}$.
\item $(u_i)_{0 \leq i < k}$ contient $k - n$ caractères $-$.
\item $(v_i)_{0 \leq i < k}$ contient $k - m$ caractères $-$.
\end{itemize}

Exemple :

\begin{tabular}{cccccccccc}
  a & a & b & c & a & a & b & a & b & a \\
  - & a & b & d & a & c & b & a & - & -
\end{tabular}

Un alignement peut être vu comme des opérations élémentaires (insertion, délétion, transformation) permettant de passer d'un mot à un autre.

\paragraph{Définition: Coût d'un alignement} Soit $(u_i)_{0 \leq i < k}$ et $(v_i)_{0 \leq i < k}$ formant un alignement de $(x_i)_{0 \leq i < n}$ et de $(y_i)_{0 \leq i < m}$.
Le \emph{coût} de cet alignement est la grandeur :
$$ \sum_{i=0}^{k-1} c(u_i, v_i) $$
Où $c(a, b)$ correspond au coût positif d'aligner $a$ avec $b$.

Par exemple, on peut choisir $c(-, b) = c(a, -) = c(a, b) = 1$ pour $a \neq b$ et $c(a, a) = 0$. Mais d'autres choix sont possibles.

Dans la suite, on s'intéresse au problème de trouver le coût minimal d'un alignement de deux séquences.

\section{Développement proprement dit}

\subsection{Relation de récurrence}

On se fixe dans la suite deux séquences $(x_i)_{0 \leq i < n}$ et $(y_i)_{0 \leq i < m}$ sur un alphabet $\Sigma$. Ainsi qu'une fonction de coût $c$ telle que décrite plus haut.

Soit $(u_i)_{0 \leq i < k}$ et $(v_i)_{0 \leq i < k}$ formant un alignement de $x$ et de $y$ optimal pour le coût $c$. Comme $c(-, -) \geq 0$, on peut supposer qu'il n'existe aucun $i < k$ tel que $u_i = v_i = 1$.

Intéressons nous au couple $(u_{k-1}, v_{k-1})$. On a trois possibilités:
\begin{itemize}
\item $(u_{k-1}, v_{k-1}) = (-, y_m)$ auquel cas $(u_i)_{0 \leq i < k-1}$ et $(v_i)_{0 \leq i < k-1}$ forment un alignement optimal de $(x_i)_{0 \leq i < n}$ et de $(y_i)_{0 \leq i < m-1}$.
\item $(u_{k-1}, v_{k-1}) = (x_n, -)$ auquel cas $(u_i)_{0 \leq i < k-1}$ et $(v_i)_{0 \leq i < k-1}$ forment un alignement optimal de $(x_i)_{0 \leq i < n-1}$ et de $(y_i)_{0 \leq i < m}$.
\item $(u_{k-1}, v_{k-1}) = (x_n, y_n)$ auquel cas $(u_i)_{0 \leq i < k-1}$ et $(v_i)_{0 \leq i < k-1}$ forment un alignement optimal de $(x_i)_{0 \leq i < n-1}$ et de $(y_i)_{0 \leq i < m-1}$.
\end{itemize}

En appelant $e(k, l)$ le coût minimal d'un alignement de $(x_i)_{0 \leq i < k}$ et de 
$(y_i)_{0 \leq i < l}$, on obtient la relation récurrente pour $0 < k < n$ et $0 < l < m$:
\begin{itemize}
\item $e(0, l) = \sum_{i=0}^{l-1} c(-, y_i)$
\item $e(k, 0) = \sum_{i=0}^{k-1} c(x_i, -)$
\item $e(k, l) = \min \{c(-, y_{l-1}) + e(k, l-1), c(x_{k-1}, -) + e(k-1, l), c(x_{k-1}, y_{l-1}) + e(k-1, l-1) \}$
\end{itemize}

\subsection{Algorithme}

On utilise la programmation dynamique.

L'algorithme utilise un tableau \texttt{E[0..n-1, 0..m-1]} qui contiendra les valeurs de $e(i, j)$ correspondantes.

\begin{algorithm}[H]
  \KwData{X[0..n-1], Y[0..m-1]}
  \KwResult{Coût d'un alignement optimal}
  \BlankLine
  \emph{Déclaration et initialisation de E}\;
  $\mbox{E}[0\dots n-1, 0\dots m-1]$\;
  $\mbox{E}[0, 0] \leftarrow 0$\;
  \For{$i\leftarrow 1$ \KwTo $n-1$}{
    $\mbox{E}[i, 0] \leftarrow \mbox{E}[i-1, 0] + c(\mbox{X}[i], -)$\;
  }
  \For{$j\leftarrow 1$ \KwTo $m-1$}{
    $\mbox{E}[0, j] \leftarrow \mbox{E}[0, j-1] + c(-, \mbox{Y}[j])$\;
  }
  \emph{Calcul du reste de E}\;
  \For{$i\leftarrow 1$ \KwTo $n-1$}{
    \For{$j\leftarrow 1$ \KwTo $m-1$}{
      $e_1 = \mbox{E}[i, j-1] + c(-, \mbox{Y}[j])$\;
      $e_2 = \mbox{E}[i-1, j] + c(\mbox{X}[i], -)$\;
      $e_3 = \mbox{E}[i-1, j-1] + c(\mbox{X}[i], \mbox{Y}[j])$\;
      $\mbox{E}[i, j] \leftarrow \min \{e_1, e_2, e_3\}$\; 
    }
  }
  \KwRet $\mbox{E}[n-1, m-1]$\;
  
\end{algorithm}

La complexité de l'algorithme est en $\mathcal{O}(n * m)$.

\subsection{Remarques de culture et d'application sur l'algorithme}

On peut récupérer un alignement optimal à partir du tableau \texttt{E}: il suffit de `remonter' les opérations à partir de l'élément \texttt{E}[$n-1, m-1$].

La distance de Levenstein s'obtient en prenant $c$ comme décrit dans le plan.

On peut retrouver la plus longue sous-séquence commune à $x$ et à $y$ en interdisant l'opération de transformation d'une lettre en une autre : pour deux lettres $a$ et $b$ distinctes:
\begin{itemize}
\item $c(-, b) = c(a, -) = 1$
\item $c(a, a) = 0$
\item $c(a, b) = +\infty$
\end{itemize}
En pratique, on peut remplacer le $+\infty$ par un nombre supérieur à $2$ (une transformation peut s'effectuer avec une délétion suivie d'une insertion).
La plus longue sous-séquence commune correspond alors aux termes de l'alignement où $u$ est égal à $v$.

Lors de l'alignement de séquences ADN, on peut avoir besoin de mettre des poids différents aux différentes transformations: certaines erreurs de recopie sont plus probables que d'autres.

Plusieurs améliorations de l'algorithme existent:
\begin{itemize}
\item On peut se contenter de ne garder qu'une ligne à la fois dans le tableau \texttt{E}, on gagne ainsi en complexité spatiale, mais on perd la possibilité de retrouver l'alignement.
  
\item L'algorithme de Hunt-Szymanski est plus efficace pour le calcul de la distance de Levenstein dans le cas où le nombre $r$ de couples $(i, j)$ avec $x_i = y_j$ est petit (souvent le cas en pratique), sa complexité est en $\mathcal{O}(n+r\log(n))$ (Crochemore).

\item Dans le cas où le coût d'une insertion ou d'une délétion est non nul, on peut restreindre \texttt{E} à une `zone diagonale'. Même en lançant plusieurs fois l'algorithme pour des largeurs de diagonale croissantes, on obtient une complexité spatiale et temporelle en $\mathcal{O}(\frac{d}{\Delta} n)$ où $d$ est le résultat de l'algorithme et $\Delta$ le plus petit coût d'une insertion ou d'une délétion (MBCT).
\end{itemize}

\section{Références}

\emph{Text Algorithms}, Maxime Crochemore

\emph{Genome-Scale Algorithm Design}, Veli Mäkinen, Djamal Belazzougui, Fabio Cunial, Alexandru I. Tomescu

\end{document}
